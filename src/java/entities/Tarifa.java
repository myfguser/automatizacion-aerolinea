/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author ASSUS
 */
@Entity
@Table(name = "tarifa")
@XmlRootElement
@NamedQueries({
  @NamedQuery(name = "Tarifa.findAll", query = "SELECT t FROM Tarifa t"),
  @NamedQuery(name = "Tarifa.findById", query = "SELECT t FROM Tarifa t WHERE t.id = :id"),
  @NamedQuery(name = "Tarifa.findByTarifa", query = "SELECT t FROM Tarifa t WHERE t.tarifa = :tarifa"),
  @NamedQuery(name = "Tarifa.findByCosto", query = "SELECT t FROM Tarifa t WHERE t.costo = :costo")})
public class Tarifa implements Serializable {
  private static final long serialVersionUID = 1L;
  @Id
  @Basic(optional = false)
  @NotNull
  @Column(name = "ID")
  private Integer id;
  @Size(max = 45)
  @Column(name = "TARIFA")
  private String tarifa;
  // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
  @Column(name = "COSTO")
  private Float costo;
  @OneToMany(cascade = CascadeType.ALL, mappedBy = "tarifa")
  private Collection<VueloHasTarifa> vueloHasTarifaCollection;

  public Tarifa() {
  }

  public Tarifa(Integer id) {
    this.id = id;
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getTarifa() {
    return tarifa;
  }

  public void setTarifa(String tarifa) {
    this.tarifa = tarifa;
  }

  public Float getCosto() {
    return costo;
  }

  public void setCosto(Float costo) {
    this.costo = costo;
  }

  @XmlTransient
  public Collection<VueloHasTarifa> getVueloHasTarifaCollection() {
    return vueloHasTarifaCollection;
  }

  public void setVueloHasTarifaCollection(Collection<VueloHasTarifa> vueloHasTarifaCollection) {
    this.vueloHasTarifaCollection = vueloHasTarifaCollection;
  }

  @Override
  public int hashCode() {
    int hash = 0;
    hash += (id != null ? id.hashCode() : 0);
    return hash;
  }

  @Override
  public boolean equals(Object object) {
    // TODO: Warning - this method won't work in the case the id fields are not set
    if (!(object instanceof Tarifa)) {
      return false;
    }
    Tarifa other = (Tarifa) object;
    if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
      return false;
    }
    return true;
  }

  @Override
  public String toString() {
    return "entities.Tarifa[ id=" + id + " ]";
  }
  
}
