/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author ASSUS
 */
@Entity
@Table(name = "aerolinea")
@XmlRootElement
@NamedQueries({
  @NamedQuery(name = "Aerolinea.findAll", query = "SELECT a FROM Aerolinea a"),
  @NamedQuery(name = "Aerolinea.findById", query = "SELECT a FROM Aerolinea a WHERE a.id = :id"),
  @NamedQuery(name = "Aerolinea.findByNombre", query = "SELECT a FROM Aerolinea a WHERE a.nombre = :nombre"),
  @NamedQuery(name = "Aerolinea.findByDescripcion", query = "SELECT a FROM Aerolinea a WHERE a.descripcion = :descripcion")})
public class Aerolinea implements Serializable {
  private static final long serialVersionUID = 1L;
  @Id
  @Basic(optional = false)
  @NotNull
  @Column(name = "ID")
  private Integer id;
  @Basic(optional = false)
  @NotNull
  @Size(min = 1, max = 45)
  @Column(name = "NOMBRE")
  private String nombre;
  @Size(max = 250)
  @Column(name = "DESCRIPCION")
  private String descripcion;
  @OneToMany(cascade = CascadeType.ALL, mappedBy = "aerolineaId")
  private Collection<Vuelo> vueloCollection;

  public Aerolinea() {
  }

  public Aerolinea(Integer id) {
    this.id = id;
  }

  public Aerolinea(Integer id, String nombre) {
    this.id = id;
    this.nombre = nombre;
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getNombre() {
    return nombre;
  }

  public void setNombre(String nombre) {
    this.nombre = nombre;
  }

  public String getDescripcion() {
    return descripcion;
  }

  public void setDescripcion(String descripcion) {
    this.descripcion = descripcion;
  }

  @XmlTransient
  public Collection<Vuelo> getVueloCollection() {
    return vueloCollection;
  }

  public void setVueloCollection(Collection<Vuelo> vueloCollection) {
    this.vueloCollection = vueloCollection;
  }

  @Override
  public int hashCode() {
    int hash = 0;
    hash += (id != null ? id.hashCode() : 0);
    return hash;
  }

  @Override
  public boolean equals(Object object) {
    // TODO: Warning - this method won't work in the case the id fields are not set
    if (!(object instanceof Aerolinea)) {
      return false;
    }
    Aerolinea other = (Aerolinea) object;
    if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
      return false;
    }
    return true;
  }

  @Override
  public String toString() {
    return "entities.Aerolinea[ id=" + id + " ]";
  }
  
}
