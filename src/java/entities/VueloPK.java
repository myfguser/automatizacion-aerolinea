/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author ASSUS
 */
@Embeddable
public class VueloPK implements Serializable {
  @Basic(optional = false)
  @NotNull
  @Column(name = "NRO_REF")
  private int nroRef;
  @Basic(optional = false)
  @NotNull
  @Column(name = "CIUDAD_ORIGEN")
  private int ciudadOrigen;
  @Basic(optional = false)
  @NotNull
  @Column(name = "CIUDAD_DESTINO")
  private int ciudadDestino;

  public VueloPK() {
  }

  public VueloPK(int nroRef, int ciudadOrigen, int ciudadDestino) {
    this.nroRef = nroRef;
    this.ciudadOrigen = ciudadOrigen;
    this.ciudadDestino = ciudadDestino;
  }

  public int getNroRef() {
    return nroRef;
  }

  public void setNroRef(int nroRef) {
    this.nroRef = nroRef;
  }

  public int getCiudadOrigen() {
    return ciudadOrigen;
  }

  public void setCiudadOrigen(int ciudadOrigen) {
    this.ciudadOrigen = ciudadOrigen;
  }

  public int getCiudadDestino() {
    return ciudadDestino;
  }

  public void setCiudadDestino(int ciudadDestino) {
    this.ciudadDestino = ciudadDestino;
  }

  @Override
  public int hashCode() {
    int hash = 0;
    hash += (int) nroRef;
    hash += (int) ciudadOrigen;
    hash += (int) ciudadDestino;
    return hash;
  }

  @Override
  public boolean equals(Object object) {
    // TODO: Warning - this method won't work in the case the id fields are not set
    if (!(object instanceof VueloPK)) {
      return false;
    }
    VueloPK other = (VueloPK) object;
    if (this.nroRef != other.nroRef) {
      return false;
    }
    if (this.ciudadOrigen != other.ciudadOrigen) {
      return false;
    }
    if (this.ciudadDestino != other.ciudadDestino) {
      return false;
    }
    return true;
  }

  @Override
  public String toString() {
    return "entities.VueloPK[ nroRef=" + nroRef + ", ciudadOrigen=" + ciudadOrigen + ", ciudadDestino=" + ciudadDestino + " ]";
  }
  
}
