/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author ASSUS
 */
@Entity
@Table(name = "tarifas_aerolinea")
@XmlRootElement
@NamedQueries({
  @NamedQuery(name = "TarifasAerolinea.findAll", query = "SELECT t FROM TarifasAerolinea t"),
  @NamedQuery(name = "TarifasAerolinea.findByFechaAplicaTarifa", query = "SELECT t FROM TarifasAerolinea t WHERE t.fechaAplicaTarifa = :fechaAplicaTarifa"),
  @NamedQuery(name = "TarifasAerolinea.findByVueloNroRef", query = "SELECT t FROM TarifasAerolinea t WHERE t.vueloNroRef = :vueloNroRef"),
  @NamedQuery(name = "TarifasAerolinea.findByIdTarifa", query = "SELECT t FROM TarifasAerolinea t WHERE t.idTarifa = :idTarifa"),
  @NamedQuery(name = "TarifasAerolinea.findByNombreTarifa", query = "SELECT t FROM TarifasAerolinea t WHERE t.nombreTarifa = :nombreTarifa"),
  @NamedQuery(name = "TarifasAerolinea.findByCosto", query = "SELECT t FROM TarifasAerolinea t WHERE t.costo = :costo")})
public class TarifasAerolinea implements Serializable {
  private static final long serialVersionUID = 1L;
  @Basic(optional = false)
  @NotNull
  @Column(name = "FECHA_APLICA_TARIFA")
  @Temporal(TemporalType.TIMESTAMP)
  private Date fechaAplicaTarifa;
  @Basic(optional = false)
  @NotNull
  @Column(name = "VUELO_NRO_REF")
  private int vueloNroRef;
  @Basic(optional = false)
  @NotNull
  @Column(name = "ID_TARIFA")
  private int idTarifa;
  @Size(max = 45)
  @Column(name = "NOMBRE_TARIFA")
  private String nombreTarifa;
  // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
  @Column(name = "COSTO")
  private Float costo;

  public TarifasAerolinea() {
  }

  public Date getFechaAplicaTarifa() {
    return fechaAplicaTarifa;
  }

  public void setFechaAplicaTarifa(Date fechaAplicaTarifa) {
    this.fechaAplicaTarifa = fechaAplicaTarifa;
  }

  public int getVueloNroRef() {
    return vueloNroRef;
  }

  public void setVueloNroRef(int vueloNroRef) {
    this.vueloNroRef = vueloNroRef;
  }

  public int getIdTarifa() {
    return idTarifa;
  }

  public void setIdTarifa(int idTarifa) {
    this.idTarifa = idTarifa;
  }

  public String getNombreTarifa() {
    return nombreTarifa;
  }

  public void setNombreTarifa(String nombreTarifa) {
    this.nombreTarifa = nombreTarifa;
  }

  public Float getCosto() {
    return costo;
  }

  public void setCosto(Float costo) {
    this.costo = costo;
  }
  
}
