/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

/**
 *
 * @author ASSUS
 */
@Embeddable
public class ReservaPK implements Serializable {
  @Basic(optional = false)
  @Column(name = "ID")
  private int id;
  @Basic(optional = false)
  @NotNull
  @Column(name = "FECHA")
  @Temporal(TemporalType.TIMESTAMP)
  private Date fecha;

  public ReservaPK() {
  }

  public ReservaPK(int id, Date fecha) {
    this.id = id;
    this.fecha = fecha;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public Date getFecha() {
    return fecha;
  }

  public void setFecha(Date fecha) {
    this.fecha = fecha;
  }

  @Override
  public int hashCode() {
    int hash = 0;
    hash += (int) id;
    hash += (fecha != null ? fecha.hashCode() : 0);
    return hash;
  }

  @Override
  public boolean equals(Object object) {
    // TODO: Warning - this method won't work in the case the id fields are not set
    if (!(object instanceof ReservaPK)) {
      return false;
    }
    ReservaPK other = (ReservaPK) object;
    if (this.id != other.id) {
      return false;
    }
    if ((this.fecha == null && other.fecha != null) || (this.fecha != null && !this.fecha.equals(other.fecha))) {
      return false;
    }
    return true;
  }

  @Override
  public String toString() {
    return "entities.ReservaPK[ id=" + id + ", fecha=" + fecha + " ]";
  }
  
}
