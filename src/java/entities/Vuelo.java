/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author ASSUS
 */
@Entity
@Table(name = "vuelo")
@XmlRootElement
@NamedQueries({
  @NamedQuery(name = "Vuelo.findAll", query = "SELECT v FROM Vuelo v"),
  @NamedQuery(name = "Vuelo.findByNroRef", query = "SELECT v FROM Vuelo v WHERE v.vueloPK.nroRef = :nroRef"),
  @NamedQuery(name = "Vuelo.findByCiudadOrigen", query = "SELECT v FROM Vuelo v WHERE v.vueloPK.ciudadOrigen = :ciudadOrigen"),
  @NamedQuery(name = "Vuelo.findByCiudadDestino", query = "SELECT v FROM Vuelo v WHERE v.vueloPK.ciudadDestino = :ciudadDestino"),
  @NamedQuery(name = "Vuelo.findByHorarioSalida", query = "SELECT v FROM Vuelo v WHERE v.horarioSalida = :horarioSalida"),
  @NamedQuery(name = "Vuelo.findByNroAsientos", query = "SELECT v FROM Vuelo v WHERE v.nroAsientos = :nroAsientos"),
  @NamedQuery(name = "Vuelo.findByRefAvion", query = "SELECT v FROM Vuelo v WHERE v.refAvion = :refAvion")})
public class Vuelo implements Serializable {
  private static final long serialVersionUID = 1L;
  @EmbeddedId
  protected VueloPK vueloPK;
  @Basic(optional = false)
  @NotNull
  @Column(name = "HORARIO_SALIDA")
  @Temporal(TemporalType.TIMESTAMP)
  private Date horarioSalida;
  @Basic(optional = false)
  @NotNull
  @Column(name = "NRO_ASIENTOS")
  private int nroAsientos;
  @Size(max = 45)
  @Column(name = "REF_AVION")
  private String refAvion;
  @OneToMany(cascade = CascadeType.ALL, mappedBy = "vueloNroRef")
  private Collection<VueloHasEstadoVuelos> vueloHasEstadoVuelosCollection;
  @OneToMany(cascade = CascadeType.ALL, mappedBy = "vuelo")
  private Collection<VueloHasTarifa> vueloHasTarifaCollection;
  @OneToMany(cascade = CascadeType.ALL, mappedBy = "vueloNroRef")
  private Collection<Reserva> reservaCollection;
  @JoinColumn(name = "AEROLINEA_ID", referencedColumnName = "ID")
  @ManyToOne(optional = false)
  private Aerolinea aerolineaId;
  @JoinColumn(name = "CIUDAD_ORIGEN", referencedColumnName = "ID", insertable = false, updatable = false)
  @ManyToOne(optional = false)
  private Ciudad ciudad;
  @JoinColumn(name = "CIUDAD_DESTINO", referencedColumnName = "ID", insertable = false, updatable = false)
  @ManyToOne(optional = false)
  private Ciudad ciudad1;

  public Vuelo() {
  }

  public Vuelo(VueloPK vueloPK) {
    this.vueloPK = vueloPK;
  }

  public Vuelo(VueloPK vueloPK, Date horarioSalida, int nroAsientos) {
    this.vueloPK = vueloPK;
    this.horarioSalida = horarioSalida;
    this.nroAsientos = nroAsientos;
  }

  public Vuelo(int nroRef, int ciudadOrigen, int ciudadDestino) {
    this.vueloPK = new VueloPK(nroRef, ciudadOrigen, ciudadDestino);
  }

  public VueloPK getVueloPK() {
    return vueloPK;
  }

  public void setVueloPK(VueloPK vueloPK) {
    this.vueloPK = vueloPK;
  }

  public Date getHorarioSalida() {
    return horarioSalida;
  }

  public void setHorarioSalida(Date horarioSalida) {
    this.horarioSalida = horarioSalida;
  }

  public int getNroAsientos() {
    return nroAsientos;
  }

  public void setNroAsientos(int nroAsientos) {
    this.nroAsientos = nroAsientos;
  }

  public String getRefAvion() {
    return refAvion;
  }

  public void setRefAvion(String refAvion) {
    this.refAvion = refAvion;
  }

  @XmlTransient
  public Collection<VueloHasEstadoVuelos> getVueloHasEstadoVuelosCollection() {
    return vueloHasEstadoVuelosCollection;
  }

  public void setVueloHasEstadoVuelosCollection(Collection<VueloHasEstadoVuelos> vueloHasEstadoVuelosCollection) {
    this.vueloHasEstadoVuelosCollection = vueloHasEstadoVuelosCollection;
  }

  @XmlTransient
  public Collection<VueloHasTarifa> getVueloHasTarifaCollection() {
    return vueloHasTarifaCollection;
  }

  public void setVueloHasTarifaCollection(Collection<VueloHasTarifa> vueloHasTarifaCollection) {
    this.vueloHasTarifaCollection = vueloHasTarifaCollection;
  }

  @XmlTransient
  public Collection<Reserva> getReservaCollection() {
    return reservaCollection;
  }

  public void setReservaCollection(Collection<Reserva> reservaCollection) {
    this.reservaCollection = reservaCollection;
  }

  public Aerolinea getAerolineaId() {
    return aerolineaId;
  }

  public void setAerolineaId(Aerolinea aerolineaId) {
    this.aerolineaId = aerolineaId;
  }

  public Ciudad getCiudad() {
    return ciudad;
  }

  public void setCiudad(Ciudad ciudad) {
    this.ciudad = ciudad;
  }

  public Ciudad getCiudad1() {
    return ciudad1;
  }

  public void setCiudad1(Ciudad ciudad1) {
    this.ciudad1 = ciudad1;
  }

  @Override
  public int hashCode() {
    int hash = 0;
    hash += (vueloPK != null ? vueloPK.hashCode() : 0);
    return hash;
  }

  @Override
  public boolean equals(Object object) {
    // TODO: Warning - this method won't work in the case the id fields are not set
    if (!(object instanceof Vuelo)) {
      return false;
    }
    Vuelo other = (Vuelo) object;
    if ((this.vueloPK == null && other.vueloPK != null) || (this.vueloPK != null && !this.vueloPK.equals(other.vueloPK))) {
      return false;
    }
    return true;
  }

  @Override
  public String toString() {
    return "entities.Vuelo[ vueloPK=" + vueloPK + " ]";
  }
  
}
