/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author ASSUS
 */
@Entity
@Table(name = "vuelo_has_estado_vuelos")
@XmlRootElement
@NamedQueries({
  @NamedQuery(name = "VueloHasEstadoVuelos.findAll", query = "SELECT v FROM VueloHasEstadoVuelos v"),
  @NamedQuery(name = "VueloHasEstadoVuelos.findById", query = "SELECT v FROM VueloHasEstadoVuelos v WHERE v.id = :id"),
  @NamedQuery(name = "VueloHasEstadoVuelos.findByTiempo", query = "SELECT v FROM VueloHasEstadoVuelos v WHERE v.tiempo = :tiempo")})
public class VueloHasEstadoVuelos implements Serializable {
  private static final long serialVersionUID = 1L;
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Basic(optional = false)
  @Column(name = "ID")
  private Integer id;
  @Basic(optional = false)
  @NotNull
  @Column(name = "TIEMPO")
  @Temporal(TemporalType.TIMESTAMP)
  private Date tiempo;
  @JoinColumn(name = "ESTADO_VUELOS_ID", referencedColumnName = "ID")
  @ManyToOne(optional = false)
  private EstadoVuelos estadoVuelosId;
  @JoinColumn(name = "VUELO_NRO_REF", referencedColumnName = "NRO_REF")
  @ManyToOne(optional = false)
  private Vuelo vueloNroRef;

  public VueloHasEstadoVuelos() {
  }

  public VueloHasEstadoVuelos(Integer id) {
    this.id = id;
  }

  public VueloHasEstadoVuelos(Integer id, Date tiempo) {
    this.id = id;
    this.tiempo = tiempo;
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public Date getTiempo() {
    return tiempo;
  }

  public void setTiempo(Date tiempo) {
    this.tiempo = tiempo;
  }

  public EstadoVuelos getEstadoVuelosId() {
    return estadoVuelosId;
  }

  public void setEstadoVuelosId(EstadoVuelos estadoVuelosId) {
    this.estadoVuelosId = estadoVuelosId;
  }

  public Vuelo getVueloNroRef() {
    return vueloNroRef;
  }

  public void setVueloNroRef(Vuelo vueloNroRef) {
    this.vueloNroRef = vueloNroRef;
  }

  @Override
  public int hashCode() {
    int hash = 0;
    hash += (id != null ? id.hashCode() : 0);
    return hash;
  }

  @Override
  public boolean equals(Object object) {
    // TODO: Warning - this method won't work in the case the id fields are not set
    if (!(object instanceof VueloHasEstadoVuelos)) {
      return false;
    }
    VueloHasEstadoVuelos other = (VueloHasEstadoVuelos) object;
    if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
      return false;
    }
    return true;
  }

  @Override
  public String toString() {
    return "entities.VueloHasEstadoVuelos[ id=" + id + " ]";
  }
  
}
