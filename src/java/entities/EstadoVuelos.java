/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author ASSUS
 */
@Entity
@Table(name = "estado_vuelos")
@XmlRootElement
@NamedQueries({
  @NamedQuery(name = "EstadoVuelos.findAll", query = "SELECT e FROM EstadoVuelos e"),
  @NamedQuery(name = "EstadoVuelos.findById", query = "SELECT e FROM EstadoVuelos e WHERE e.id = :id"),
  @NamedQuery(name = "EstadoVuelos.findByEstado", query = "SELECT e FROM EstadoVuelos e WHERE e.estado = :estado")})
public class EstadoVuelos implements Serializable {
  private static final long serialVersionUID = 1L;
  @Id
  @Basic(optional = false)
  @NotNull
  @Column(name = "ID")
  private Integer id;
  @Basic(optional = false)
  @NotNull
  @Size(min = 1, max = 45)
  @Column(name = "ESTADO")
  private String estado;
  @OneToMany(cascade = CascadeType.ALL, mappedBy = "estadoVuelosId")
  private Collection<VueloHasEstadoVuelos> vueloHasEstadoVuelosCollection;

  public EstadoVuelos() {
  }

  public EstadoVuelos(Integer id) {
    this.id = id;
  }

  public EstadoVuelos(Integer id, String estado) {
    this.id = id;
    this.estado = estado;
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getEstado() {
    return estado;
  }

  public void setEstado(String estado) {
    this.estado = estado;
  }

  @XmlTransient
  public Collection<VueloHasEstadoVuelos> getVueloHasEstadoVuelosCollection() {
    return vueloHasEstadoVuelosCollection;
  }

  public void setVueloHasEstadoVuelosCollection(Collection<VueloHasEstadoVuelos> vueloHasEstadoVuelosCollection) {
    this.vueloHasEstadoVuelosCollection = vueloHasEstadoVuelosCollection;
  }

  @Override
  public int hashCode() {
    int hash = 0;
    hash += (id != null ? id.hashCode() : 0);
    return hash;
  }

  @Override
  public boolean equals(Object object) {
    // TODO: Warning - this method won't work in the case the id fields are not set
    if (!(object instanceof EstadoVuelos)) {
      return false;
    }
    EstadoVuelos other = (EstadoVuelos) object;
    if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
      return false;
    }
    return true;
  }

  @Override
  public String toString() {
    return "entities.EstadoVuelos[ id=" + id + " ]";
  }
  
}
