/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author ASSUS
 */
@Entity
@Table(name = "vuelos_sin_cerrar")
@XmlRootElement
@NamedQueries({
  @NamedQuery(name = "VuelosSinCerrar.findAll", query = "SELECT v FROM VuelosSinCerrar v"),
  @NamedQuery(name = "VuelosSinCerrar.findById", query = "SELECT v FROM VuelosSinCerrar v WHERE v.id = :id"),
  @NamedQuery(name = "VuelosSinCerrar.findByEstado", query = "SELECT v FROM VuelosSinCerrar v WHERE v.estado = :estado"),
  @NamedQuery(name = "VuelosSinCerrar.findByVueloNroRef", query = "SELECT v FROM VuelosSinCerrar v WHERE v.vueloNroRef = :vueloNroRef")})
public class VuelosSinCerrar implements Serializable {
  private static final long serialVersionUID = 1L;
  @Basic(optional = false)
  @NotNull
  @Column(name = "ID")
  private int id;
  @Basic(optional = false)
  @NotNull
  @Size(min = 1, max = 45)
  @Column(name = "ESTADO")
  private String estado;
  @Basic(optional = false)
  @NotNull
  @Column(name = "VUELO_NRO_REF")
  private int vueloNroRef;

  public VuelosSinCerrar() {
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getEstado() {
    return estado;
  }

  public void setEstado(String estado) {
    this.estado = estado;
  }

  public int getVueloNroRef() {
    return vueloNroRef;
  }

  public void setVueloNroRef(int vueloNroRef) {
    this.vueloNroRef = vueloNroRef;
  }
  
}
