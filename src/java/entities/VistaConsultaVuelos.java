/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author ASSUS
 */
@Entity
@Table(name = "vista_consulta_vuelos")
@XmlRootElement
@NamedQueries({
  @NamedQuery(name = "VistaConsultaVuelos.findAll", query = "SELECT v FROM VistaConsultaVuelos v"),
  @NamedQuery(name = "VistaConsultaVuelos.findByIdCiudadOrigen", query = "SELECT v FROM VistaConsultaVuelos v WHERE v.idCiudadOrigen = :idCiudadOrigen"),
  @NamedQuery(name = "VistaConsultaVuelos.findByNombreCiudadOrigen", query = "SELECT v FROM VistaConsultaVuelos v WHERE v.nombreCiudadOrigen = :nombreCiudadOrigen"),
  @NamedQuery(name = "VistaConsultaVuelos.findByIdCiudadDestino", query = "SELECT v FROM VistaConsultaVuelos v WHERE v.idCiudadDestino = :idCiudadDestino"),
  @NamedQuery(name = "VistaConsultaVuelos.findByNombreCiudadDestino", query = "SELECT v FROM VistaConsultaVuelos v WHERE v.nombreCiudadDestino = :nombreCiudadDestino"),
  @NamedQuery(name = "VistaConsultaVuelos.findByIdAerolinea", query = "SELECT v FROM VistaConsultaVuelos v WHERE v.idAerolinea = :idAerolinea"),
  @NamedQuery(name = "VistaConsultaVuelos.findByNombreAerolinea", query = "SELECT v FROM VistaConsultaVuelos v WHERE v.nombreAerolinea = :nombreAerolinea"),
  @NamedQuery(name = "VistaConsultaVuelos.findByIdentificacionReferenciaVuelo", query = "SELECT v FROM VistaConsultaVuelos v WHERE v.identificacionReferenciaVuelo = :identificacionReferenciaVuelo"),
  @NamedQuery(name = "VistaConsultaVuelos.findByHorarioSalida", query = "SELECT v FROM VistaConsultaVuelos v WHERE v.horarioSalida = :horarioSalida"),
  @NamedQuery(name = "VistaConsultaVuelos.findByCantidadAsientos", query = "SELECT v FROM VistaConsultaVuelos v WHERE v.cantidadAsientos = :cantidadAsientos"),
  @NamedQuery(name = "VistaConsultaVuelos.findByCantidadAsientosReservados", query = "SELECT v FROM VistaConsultaVuelos v WHERE v.cantidadAsientosReservados = :cantidadAsientosReservados"),
  @NamedQuery(name = "VistaConsultaVuelos.findByAsientosDisponibles", query = "SELECT v FROM VistaConsultaVuelos v WHERE v.asientosDisponibles = :asientosDisponibles"),
  @NamedQuery(name = "VistaConsultaVuelos.findByEstadoVuelo", query = "SELECT v FROM VistaConsultaVuelos v WHERE v.estadoVuelo = :estadoVuelo")})
public class VistaConsultaVuelos implements Serializable {
  private static final long serialVersionUID = 1L;
  @Basic(optional = false)
  @NotNull
  @Column(name = "ID_CIUDAD_ORIGEN")
  private int idCiudadOrigen;
  @Basic(optional = false)
  @NotNull
  @Size(min = 1, max = 150)
  @Column(name = "NOMBRE_CIUDAD_ORIGEN")
  private String nombreCiudadOrigen;
  @Basic(optional = false)
  @NotNull
  @Column(name = "ID_CIUDAD_DESTINO")
  private int idCiudadDestino;
  @Basic(optional = false)
  @NotNull
  @Size(min = 1, max = 150)
  @Column(name = "NOMBRE_CIUDAD_DESTINO")
  private String nombreCiudadDestino;
  @Basic(optional = false)
  @NotNull
  @Column(name = "ID_AEROLINEA")
  private int idAerolinea;
  @Basic(optional = false)
  @NotNull
  @Size(min = 1, max = 45)
  @Column(name = "NOMBRE_AEROLINEA")
  private String nombreAerolinea;
  @Basic(optional = false)
  @NotNull
  @Column(name = "IDENTIFICACION_REFERENCIA_VUELO")
  private int identificacionReferenciaVuelo;
  @Basic(optional = false)
  @NotNull
  @Column(name = "HORARIO_SALIDA")
  @Temporal(TemporalType.TIMESTAMP)
  private Date horarioSalida;
  @Basic(optional = false)
  @NotNull
  @Column(name = "CANTIDAD_ASIENTOS")
  private int cantidadAsientos;
  @Basic(optional = false)
  @NotNull
  @Column(name = "CANTIDAD_ASIENTOS_RESERVADOS")
  private long cantidadAsientosReservados;
  @Basic(optional = false)
  @NotNull
  @Column(name = "ASIENTOS_DISPONIBLES")
  private long asientosDisponibles;
  @Basic(optional = false)
  @NotNull
  @Size(min = 1, max = 45)
  @Column(name = "ESTADO_VUELO")
  private String estadoVuelo;

  public VistaConsultaVuelos() {
  }

  public int getIdCiudadOrigen() {
    return idCiudadOrigen;
  }

  public void setIdCiudadOrigen(int idCiudadOrigen) {
    this.idCiudadOrigen = idCiudadOrigen;
  }

  public String getNombreCiudadOrigen() {
    return nombreCiudadOrigen;
  }

  public void setNombreCiudadOrigen(String nombreCiudadOrigen) {
    this.nombreCiudadOrigen = nombreCiudadOrigen;
  }

  public int getIdCiudadDestino() {
    return idCiudadDestino;
  }

  public void setIdCiudadDestino(int idCiudadDestino) {
    this.idCiudadDestino = idCiudadDestino;
  }

  public String getNombreCiudadDestino() {
    return nombreCiudadDestino;
  }

  public void setNombreCiudadDestino(String nombreCiudadDestino) {
    this.nombreCiudadDestino = nombreCiudadDestino;
  }

  public int getIdAerolinea() {
    return idAerolinea;
  }

  public void setIdAerolinea(int idAerolinea) {
    this.idAerolinea = idAerolinea;
  }

  public String getNombreAerolinea() {
    return nombreAerolinea;
  }

  public void setNombreAerolinea(String nombreAerolinea) {
    this.nombreAerolinea = nombreAerolinea;
  }

  public int getIdentificacionReferenciaVuelo() {
    return identificacionReferenciaVuelo;
  }

  public void setIdentificacionReferenciaVuelo(int identificacionReferenciaVuelo) {
    this.identificacionReferenciaVuelo = identificacionReferenciaVuelo;
  }

  public Date getHorarioSalida() {
    return horarioSalida;
  }

  public void setHorarioSalida(Date horarioSalida) {
    this.horarioSalida = horarioSalida;
  }

  public int getCantidadAsientos() {
    return cantidadAsientos;
  }

  public void setCantidadAsientos(int cantidadAsientos) {
    this.cantidadAsientos = cantidadAsientos;
  }

  public long getCantidadAsientosReservados() {
    return cantidadAsientosReservados;
  }

  public void setCantidadAsientosReservados(long cantidadAsientosReservados) {
    this.cantidadAsientosReservados = cantidadAsientosReservados;
  }

  public long getAsientosDisponibles() {
    return asientosDisponibles;
  }

  public void setAsientosDisponibles(long asientosDisponibles) {
    this.asientosDisponibles = asientosDisponibles;
  }

  public String getEstadoVuelo() {
    return estadoVuelo;
  }

  public void setEstadoVuelo(String estadoVuelo) {
    this.estadoVuelo = estadoVuelo;
  }
  
}
