/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author ASSUS
 */
@Embeddable
public class VueloHasTarifaPK implements Serializable {
  @Basic(optional = false)
  @Column(name = "ID")
  private int id;
  @Basic(optional = false)
  @NotNull
  @Column(name = "VUELO_NRO_REF")
  private int vueloNroRef;
  @Basic(optional = false)
  @NotNull
  @Column(name = "TARIFA_ID")
  private int tarifaId;

  public VueloHasTarifaPK() {
  }

  public VueloHasTarifaPK(int id, int vueloNroRef, int tarifaId) {
    this.id = id;
    this.vueloNroRef = vueloNroRef;
    this.tarifaId = tarifaId;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public int getVueloNroRef() {
    return vueloNroRef;
  }

  public void setVueloNroRef(int vueloNroRef) {
    this.vueloNroRef = vueloNroRef;
  }

  public int getTarifaId() {
    return tarifaId;
  }

  public void setTarifaId(int tarifaId) {
    this.tarifaId = tarifaId;
  }

  @Override
  public int hashCode() {
    int hash = 0;
    hash += (int) id;
    hash += (int) vueloNroRef;
    hash += (int) tarifaId;
    return hash;
  }

  @Override
  public boolean equals(Object object) {
    // TODO: Warning - this method won't work in the case the id fields are not set
    if (!(object instanceof VueloHasTarifaPK)) {
      return false;
    }
    VueloHasTarifaPK other = (VueloHasTarifaPK) object;
    if (this.id != other.id) {
      return false;
    }
    if (this.vueloNroRef != other.vueloNroRef) {
      return false;
    }
    if (this.tarifaId != other.tarifaId) {
      return false;
    }
    return true;
  }

  @Override
  public String toString() {
    return "entities.VueloHasTarifaPK[ id=" + id + ", vueloNroRef=" + vueloNroRef + ", tarifaId=" + tarifaId + " ]";
  }
  
}
