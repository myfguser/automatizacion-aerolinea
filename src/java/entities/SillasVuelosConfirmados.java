/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author ASSUS
 */
@Entity
@Table(name = "sillas_vuelos_confirmados")
@XmlRootElement
@NamedQueries({
  @NamedQuery(name = "SillasVuelosConfirmados.findAll", query = "SELECT s FROM SillasVuelosConfirmados s"),
  @NamedQuery(name = "SillasVuelosConfirmados.findByCantidad", query = "SELECT s FROM SillasVuelosConfirmados s WHERE s.cantidad = :cantidad"),
  @NamedQuery(name = "SillasVuelosConfirmados.findByVueloNroRef", query = "SELECT s FROM SillasVuelosConfirmados s WHERE s.vueloNroRef = :vueloNroRef")})
public class SillasVuelosConfirmados implements Serializable {
  private static final long serialVersionUID = 1L;
  @Basic(optional = false)
  @NotNull
  @Column(name = "CANTIDAD")
  private long cantidad;
  @Basic(optional = false)
  @NotNull
  @Column(name = "VUELO_NRO_REF")
  private int vueloNroRef;

  public SillasVuelosConfirmados() {
  }

  public long getCantidad() {
    return cantidad;
  }

  public void setCantidad(long cantidad) {
    this.cantidad = cantidad;
  }

  public int getVueloNroRef() {
    return vueloNroRef;
  }

  public void setVueloNroRef(int vueloNroRef) {
    this.vueloNroRef = vueloNroRef;
  }
  
}
