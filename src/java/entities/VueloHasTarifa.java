/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author ASSUS
 */
@Entity
@Table(name = "vuelo_has_tarifa")
@XmlRootElement
@NamedQueries({
  @NamedQuery(name = "VueloHasTarifa.findAll", query = "SELECT v FROM VueloHasTarifa v"),
  @NamedQuery(name = "VueloHasTarifa.findById", query = "SELECT v FROM VueloHasTarifa v WHERE v.vueloHasTarifaPK.id = :id"),
  @NamedQuery(name = "VueloHasTarifa.findByRegistro", query = "SELECT v FROM VueloHasTarifa v WHERE v.registro = :registro"),
  @NamedQuery(name = "VueloHasTarifa.findByVueloNroRef", query = "SELECT v FROM VueloHasTarifa v WHERE v.vueloHasTarifaPK.vueloNroRef = :vueloNroRef"),
  @NamedQuery(name = "VueloHasTarifa.findByTarifaId", query = "SELECT v FROM VueloHasTarifa v WHERE v.vueloHasTarifaPK.tarifaId = :tarifaId")})
public class VueloHasTarifa implements Serializable {
  private static final long serialVersionUID = 1L;
  @EmbeddedId
  protected VueloHasTarifaPK vueloHasTarifaPK;
  @Basic(optional = false)
  @NotNull
  @Column(name = "REGISTRO")
  @Temporal(TemporalType.TIMESTAMP)
  private Date registro;
  @JoinColumn(name = "TARIFA_ID", referencedColumnName = "ID", insertable = false, updatable = false)
  @ManyToOne(optional = false)
  private Tarifa tarifa;
  @JoinColumn(name = "VUELO_NRO_REF", referencedColumnName = "NRO_REF", insertable = false, updatable = false)
  @ManyToOne(optional = false)
  private Vuelo vuelo;

  public VueloHasTarifa() {
  }

  public VueloHasTarifa(VueloHasTarifaPK vueloHasTarifaPK) {
    this.vueloHasTarifaPK = vueloHasTarifaPK;
  }

  public VueloHasTarifa(VueloHasTarifaPK vueloHasTarifaPK, Date registro) {
    this.vueloHasTarifaPK = vueloHasTarifaPK;
    this.registro = registro;
  }

  public VueloHasTarifa(int id, int vueloNroRef, int tarifaId) {
    this.vueloHasTarifaPK = new VueloHasTarifaPK(id, vueloNroRef, tarifaId);
  }

  public VueloHasTarifaPK getVueloHasTarifaPK() {
    return vueloHasTarifaPK;
  }

  public void setVueloHasTarifaPK(VueloHasTarifaPK vueloHasTarifaPK) {
    this.vueloHasTarifaPK = vueloHasTarifaPK;
  }

  public Date getRegistro() {
    return registro;
  }

  public void setRegistro(Date registro) {
    this.registro = registro;
  }

  public Tarifa getTarifa() {
    return tarifa;
  }

  public void setTarifa(Tarifa tarifa) {
    this.tarifa = tarifa;
  }

  public Vuelo getVuelo() {
    return vuelo;
  }

  public void setVuelo(Vuelo vuelo) {
    this.vuelo = vuelo;
  }

  @Override
  public int hashCode() {
    int hash = 0;
    hash += (vueloHasTarifaPK != null ? vueloHasTarifaPK.hashCode() : 0);
    return hash;
  }

  @Override
  public boolean equals(Object object) {
    // TODO: Warning - this method won't work in the case the id fields are not set
    if (!(object instanceof VueloHasTarifa)) {
      return false;
    }
    VueloHasTarifa other = (VueloHasTarifa) object;
    if ((this.vueloHasTarifaPK == null && other.vueloHasTarifaPK != null) || (this.vueloHasTarifaPK != null && !this.vueloHasTarifaPK.equals(other.vueloHasTarifaPK))) {
      return false;
    }
    return true;
  }

  @Override
  public String toString() {
    return "entities.VueloHasTarifa[ vueloHasTarifaPK=" + vueloHasTarifaPK + " ]";
  }
  
}
