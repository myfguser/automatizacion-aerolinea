/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author ASSUS
 */
@Entity
@Table(name = "ciudad")
@XmlRootElement
@NamedQueries({
  @NamedQuery(name = "Ciudad.findAll", query = "SELECT c FROM Ciudad c"),
  @NamedQuery(name = "Ciudad.findById", query = "SELECT c FROM Ciudad c WHERE c.id = :id"),
  @NamedQuery(name = "Ciudad.findByCiudad", query = "SELECT c FROM Ciudad c WHERE c.ciudad = :ciudad")})
public class Ciudad implements Serializable {
  private static final long serialVersionUID = 1L;
  @Id
  @Basic(optional = false)
  @NotNull
  @Column(name = "ID")
  private Integer id;
  @Basic(optional = false)
  @NotNull
  @Size(min = 1, max = 150)
  @Column(name = "CIUDAD")
  private String ciudad;
  @OneToMany(cascade = CascadeType.ALL, mappedBy = "ciudad")
  private Collection<Vuelo> vueloCollection;
  @OneToMany(cascade = CascadeType.ALL, mappedBy = "ciudad1")
  private Collection<Vuelo> vueloCollection1;

  public Ciudad() {
  }

  public Ciudad(Integer id) {
    this.id = id;
  }

  public Ciudad(Integer id, String ciudad) {
    this.id = id;
    this.ciudad = ciudad;
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getCiudad() {
    return ciudad;
  }

  public void setCiudad(String ciudad) {
    this.ciudad = ciudad;
  }

  @XmlTransient
  public Collection<Vuelo> getVueloCollection() {
    return vueloCollection;
  }

  public void setVueloCollection(Collection<Vuelo> vueloCollection) {
    this.vueloCollection = vueloCollection;
  }

  @XmlTransient
  public Collection<Vuelo> getVueloCollection1() {
    return vueloCollection1;
  }

  public void setVueloCollection1(Collection<Vuelo> vueloCollection1) {
    this.vueloCollection1 = vueloCollection1;
  }

  @Override
  public int hashCode() {
    int hash = 0;
    hash += (id != null ? id.hashCode() : 0);
    return hash;
  }

  @Override
  public boolean equals(Object object) {
    // TODO: Warning - this method won't work in the case the id fields are not set
    if (!(object instanceof Ciudad)) {
      return false;
    }
    Ciudad other = (Ciudad) object;
    if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
      return false;
    }
    return true;
  }

  @Override
  public String toString() {
    return "entities.Ciudad[ id=" + id + " ]";
  }
  
}
