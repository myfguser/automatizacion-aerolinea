/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author ASSUS
 */
@Entity
@Table(name = "reserva")
@XmlRootElement
@NamedQueries({
  @NamedQuery(name = "Reserva.findAll", query = "SELECT r FROM Reserva r"),
  @NamedQuery(name = "Reserva.findById", query = "SELECT r FROM Reserva r WHERE r.reservaPK.id = :id"),
  @NamedQuery(name = "Reserva.findByFecha", query = "SELECT r FROM Reserva r WHERE r.reservaPK.fecha = :fecha"),
  @NamedQuery(name = "Reserva.findByEfectuada", query = "SELECT r FROM Reserva r WHERE r.efectuada = :efectuada")})
public class Reserva implements Serializable {
  private static final long serialVersionUID = 1L;
  @EmbeddedId
  protected ReservaPK reservaPK;
  @Basic(optional = false)
  @NotNull
  @Column(name = "EFECTUADA")
  private boolean efectuada;
  @JoinColumn(name = "USUARIO_ID", referencedColumnName = "ID")
  @ManyToOne(optional = false)
  private Usuario usuarioId;
  @JoinColumn(name = "VUELO_NRO_REF", referencedColumnName = "NRO_REF")
  @ManyToOne(optional = false)
  private Vuelo vueloNroRef;

  public Reserva() {
  }

  public Reserva(ReservaPK reservaPK) {
    this.reservaPK = reservaPK;
  }

  public Reserva(ReservaPK reservaPK, boolean efectuada) {
    this.reservaPK = reservaPK;
    this.efectuada = efectuada;
  }

  public Reserva(int id, Date fecha) {
    this.reservaPK = new ReservaPK(id, fecha);
  }

  public ReservaPK getReservaPK() {
    return reservaPK;
  }

  public void setReservaPK(ReservaPK reservaPK) {
    this.reservaPK = reservaPK;
  }

  public boolean getEfectuada() {
    return efectuada;
  }

  public void setEfectuada(boolean efectuada) {
    this.efectuada = efectuada;
  }

  public Usuario getUsuarioId() {
    return usuarioId;
  }

  public void setUsuarioId(Usuario usuarioId) {
    this.usuarioId = usuarioId;
  }

  public Vuelo getVueloNroRef() {
    return vueloNroRef;
  }

  public void setVueloNroRef(Vuelo vueloNroRef) {
    this.vueloNroRef = vueloNroRef;
  }

  @Override
  public int hashCode() {
    int hash = 0;
    hash += (reservaPK != null ? reservaPK.hashCode() : 0);
    return hash;
  }

  @Override
  public boolean equals(Object object) {
    // TODO: Warning - this method won't work in the case the id fields are not set
    if (!(object instanceof Reserva)) {
      return false;
    }
    Reserva other = (Reserva) object;
    if ((this.reservaPK == null && other.reservaPK != null) || (this.reservaPK != null && !this.reservaPK.equals(other.reservaPK))) {
      return false;
    }
    return true;
  }

  @Override
  public String toString() {
    return "entities.Reserva[ reservaPK=" + reservaPK + " ]";
  }
  
}
