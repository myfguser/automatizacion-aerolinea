/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author ASSUS
 */
@Entity
@Table(name = "vista_consulta_tarifa")
@XmlRootElement
@NamedQueries({
  @NamedQuery(name = "VistaConsultaTarifa.findAll", query = "SELECT v FROM VistaConsultaTarifa v"),
  @NamedQuery(name = "VistaConsultaTarifa.findByIdCiudadOrigen", query = "SELECT v FROM VistaConsultaTarifa v WHERE v.idCiudadOrigen = :idCiudadOrigen"),
  @NamedQuery(name = "VistaConsultaTarifa.findByNombreCiudadOrigen", query = "SELECT v FROM VistaConsultaTarifa v WHERE v.nombreCiudadOrigen = :nombreCiudadOrigen"),
  @NamedQuery(name = "VistaConsultaTarifa.findByIdCiudadDestino", query = "SELECT v FROM VistaConsultaTarifa v WHERE v.idCiudadDestino = :idCiudadDestino"),
  @NamedQuery(name = "VistaConsultaTarifa.findByNombreCiudadDestino", query = "SELECT v FROM VistaConsultaTarifa v WHERE v.nombreCiudadDestino = :nombreCiudadDestino"),
  @NamedQuery(name = "VistaConsultaTarifa.findByIdAerolinea", query = "SELECT v FROM VistaConsultaTarifa v WHERE v.idAerolinea = :idAerolinea"),
  @NamedQuery(name = "VistaConsultaTarifa.findByNombreAerolinea", query = "SELECT v FROM VistaConsultaTarifa v WHERE v.nombreAerolinea = :nombreAerolinea"),
  @NamedQuery(name = "VistaConsultaTarifa.findByIdentificacionReferenciaVuelo", query = "SELECT v FROM VistaConsultaTarifa v WHERE v.identificacionReferenciaVuelo = :identificacionReferenciaVuelo"),
  @NamedQuery(name = "VistaConsultaTarifa.findByHorarioSalida", query = "SELECT v FROM VistaConsultaTarifa v WHERE v.horarioSalida = :horarioSalida"),
  @NamedQuery(name = "VistaConsultaTarifa.findByEstadoVuelo", query = "SELECT v FROM VistaConsultaTarifa v WHERE v.estadoVuelo = :estadoVuelo"),
  @NamedQuery(name = "VistaConsultaTarifa.findByCosto", query = "SELECT v FROM VistaConsultaTarifa v WHERE v.costo = :costo"),
  @NamedQuery(name = "VistaConsultaTarifa.findByNombreTarifa", query = "SELECT v FROM VistaConsultaTarifa v WHERE v.nombreTarifa = :nombreTarifa"),
  @NamedQuery(name = "VistaConsultaTarifa.findByFechaAplicaTarifa", query = "SELECT v FROM VistaConsultaTarifa v WHERE v.fechaAplicaTarifa = :fechaAplicaTarifa")})
public class VistaConsultaTarifa implements Serializable {
  private static final long serialVersionUID = 1L;
  @Basic(optional = false)
  @NotNull
  @Column(name = "ID_CIUDAD_ORIGEN")
  private int idCiudadOrigen;
  @Basic(optional = false)
  @NotNull
  @Size(min = 1, max = 150)
  @Column(name = "NOMBRE_CIUDAD_ORIGEN")
  private String nombreCiudadOrigen;
  @Basic(optional = false)
  @NotNull
  @Column(name = "ID_CIUDAD_DESTINO")
  private int idCiudadDestino;
  @Basic(optional = false)
  @NotNull
  @Size(min = 1, max = 150)
  @Column(name = "NOMBRE_CIUDAD_DESTINO")
  private String nombreCiudadDestino;
  @Basic(optional = false)
  @NotNull
  @Column(name = "ID_AEROLINEA")
  private int idAerolinea;
  @Basic(optional = false)
  @NotNull
  @Size(min = 1, max = 45)
  @Column(name = "NOMBRE_AEROLINEA")
  private String nombreAerolinea;
  @Basic(optional = false)
  @NotNull
  @Column(name = "IDENTIFICACION_REFERENCIA_VUELO")
  private int identificacionReferenciaVuelo;
  @Basic(optional = false)
  @NotNull
  @Column(name = "HORARIO_SALIDA")
  @Temporal(TemporalType.TIMESTAMP)
  private Date horarioSalida;
  @Basic(optional = false)
  @NotNull
  @Size(min = 1, max = 45)
  @Column(name = "ESTADO_VUELO")
  private String estadoVuelo;
  // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
  @Column(name = "COSTO")
  private Float costo;
  @Size(max = 45)
  @Column(name = "NOMBRE_TARIFA")
  private String nombreTarifa;
  @Basic(optional = false)
  @NotNull
  @Column(name = "FECHA_APLICA_TARIFA")
  @Temporal(TemporalType.TIMESTAMP)
  private Date fechaAplicaTarifa;

  public VistaConsultaTarifa() {
  }

  public int getIdCiudadOrigen() {
    return idCiudadOrigen;
  }

  public void setIdCiudadOrigen(int idCiudadOrigen) {
    this.idCiudadOrigen = idCiudadOrigen;
  }

  public String getNombreCiudadOrigen() {
    return nombreCiudadOrigen;
  }

  public void setNombreCiudadOrigen(String nombreCiudadOrigen) {
    this.nombreCiudadOrigen = nombreCiudadOrigen;
  }

  public int getIdCiudadDestino() {
    return idCiudadDestino;
  }

  public void setIdCiudadDestino(int idCiudadDestino) {
    this.idCiudadDestino = idCiudadDestino;
  }

  public String getNombreCiudadDestino() {
    return nombreCiudadDestino;
  }

  public void setNombreCiudadDestino(String nombreCiudadDestino) {
    this.nombreCiudadDestino = nombreCiudadDestino;
  }

  public int getIdAerolinea() {
    return idAerolinea;
  }

  public void setIdAerolinea(int idAerolinea) {
    this.idAerolinea = idAerolinea;
  }

  public String getNombreAerolinea() {
    return nombreAerolinea;
  }

  public void setNombreAerolinea(String nombreAerolinea) {
    this.nombreAerolinea = nombreAerolinea;
  }

  public int getIdentificacionReferenciaVuelo() {
    return identificacionReferenciaVuelo;
  }

  public void setIdentificacionReferenciaVuelo(int identificacionReferenciaVuelo) {
    this.identificacionReferenciaVuelo = identificacionReferenciaVuelo;
  }

  public Date getHorarioSalida() {
    return horarioSalida;
  }

  public void setHorarioSalida(Date horarioSalida) {
    this.horarioSalida = horarioSalida;
  }

  public String getEstadoVuelo() {
    return estadoVuelo;
  }

  public void setEstadoVuelo(String estadoVuelo) {
    this.estadoVuelo = estadoVuelo;
  }

  public Float getCosto() {
    return costo;
  }

  public void setCosto(Float costo) {
    this.costo = costo;
  }

  public String getNombreTarifa() {
    return nombreTarifa;
  }

  public void setNombreTarifa(String nombreTarifa) {
    this.nombreTarifa = nombreTarifa;
  }

  public Date getFechaAplicaTarifa() {
    return fechaAplicaTarifa;
  }

  public void setFechaAplicaTarifa(Date fechaAplicaTarifa) {
    this.fechaAplicaTarifa = fechaAplicaTarifa;
  }
  
}
