package vistasjsf;

import entities.Vuelo;
import vistasjsf.util.JsfUtil;
import vistasjsf.util.PaginationHelper;
import vistasSession.VueloFacade;

import java.io.Serializable;
import java.util.ResourceBundle;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.faces.model.SelectItem;

@ManagedBean(name = "vueloController")
@SessionScoped
public class VueloController implements Serializable {

  private Vuelo current;
  private DataModel items = null;
  @EJB
  private vistasSession.VueloFacade ejbFacade;
  private PaginationHelper pagination;
  private int selectedItemIndex;

  public VueloController() {
  }

  public Vuelo getSelected() {
    if (current == null) {
      current = new Vuelo();
      current.setVueloPK(new entities.VueloPK());
      selectedItemIndex = -1;
    }
    return current;
  }

  private VueloFacade getFacade() {
    return ejbFacade;
  }

  public PaginationHelper getPagination() {
    if (pagination == null) {
      pagination = new PaginationHelper(10) {

        @Override
        public int getItemsCount() {
          return getFacade().count();
        }

        @Override
        public DataModel createPageDataModel() {
          return new ListDataModel(getFacade().findRange(new int[]{getPageFirstItem(), getPageFirstItem() + getPageSize()}));
        }
      };
    }
    return pagination;
  }

  public String prepareList() {
    recreateModel();
    return "List";
  }

  public String prepareView() {
    current = (Vuelo) getItems().getRowData();
    selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
    return "View";
  }

  public String prepareCreate() {
    current = new Vuelo();
    current.setVueloPK(new entities.VueloPK());
    selectedItemIndex = -1;
    return "Create";
  }

  public String create() {
    try {
      current.getVueloPK().setCiudadOrigen(current.getCiudad().getId());
      current.getVueloPK().setCiudadDestino(current.getCiudad1().getId());
      getFacade().create(current);
      JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("VueloCreated"));
      return prepareCreate();
    } catch (Exception e) {
      JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
      return null;
    }
  }

  public String prepareEdit() {
    current = (Vuelo) getItems().getRowData();
    selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
    return "Edit";
  }

  public String update() {
    try {
      current.getVueloPK().setCiudadOrigen(current.getCiudad().getId());
      current.getVueloPK().setCiudadDestino(current.getCiudad1().getId());
      getFacade().edit(current);
      JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("VueloUpdated"));
      return "View";
    } catch (Exception e) {
      JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
      return null;
    }
  }

  public String destroy() {
    current = (Vuelo) getItems().getRowData();
    selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
    performDestroy();
    recreatePagination();
    recreateModel();
    return "List";
  }

  public String destroyAndView() {
    performDestroy();
    recreateModel();
    updateCurrentItem();
    if (selectedItemIndex >= 0) {
      return "View";
    } else {
      // all items were removed - go back to list
      recreateModel();
      return "List";
    }
  }

  private void performDestroy() {
    try {
      getFacade().remove(current);
      JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("VueloDeleted"));
    } catch (Exception e) {
      JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
    }
  }

  private void updateCurrentItem() {
    int count = getFacade().count();
    if (selectedItemIndex >= count) {
      // selected index cannot be bigger than number of items:
      selectedItemIndex = count - 1;
      // go to previous page if last page disappeared:
      if (pagination.getPageFirstItem() >= count) {
        pagination.previousPage();
      }
    }
    if (selectedItemIndex >= 0) {
      current = getFacade().findRange(new int[]{selectedItemIndex, selectedItemIndex + 1}).get(0);
    }
  }

  public DataModel getItems() {
    if (items == null) {
      items = getPagination().createPageDataModel();
    }
    return items;
  }

  private void recreateModel() {
    items = null;
  }

  private void recreatePagination() {
    pagination = null;
  }

  public String next() {
    getPagination().nextPage();
    recreateModel();
    return "List";
  }

  public String previous() {
    getPagination().previousPage();
    recreateModel();
    return "List";
  }

  public SelectItem[] getItemsAvailableSelectMany() {
    return JsfUtil.getSelectItems(ejbFacade.findAll(), false);
  }

  public SelectItem[] getItemsAvailableSelectOne() {
    return JsfUtil.getSelectItems(ejbFacade.findAll(), true);
  }

  @FacesConverter(forClass = Vuelo.class)
  public static class VueloControllerConverter implements Converter {

    private static final String SEPARATOR = "#";
    private static final String SEPARATOR_ESCAPED = "\\#";

    @Override
    public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
      if (value == null || value.length() == 0) {
        return null;
      }
      VueloController controller = (VueloController) facesContext.getApplication().getELResolver().
              getValue(facesContext.getELContext(), null, "vueloController");
      return controller.ejbFacade.find(getKey(value));
    }

    entities.VueloPK getKey(String value) {
      entities.VueloPK key;
      String values[] = value.split(SEPARATOR_ESCAPED);
      key = new entities.VueloPK();
      key.setNroRef(Integer.parseInt(values[0]));
      key.setCiudadOrigen(Integer.parseInt(values[1]));
      key.setCiudadDestino(Integer.parseInt(values[2]));
      return key;
    }

    String getStringKey(entities.VueloPK value) {
      StringBuilder sb = new StringBuilder();
      sb.append(value.getNroRef());
      sb.append(SEPARATOR);
      sb.append(value.getCiudadOrigen());
      sb.append(SEPARATOR);
      sb.append(value.getCiudadDestino());
      return sb.toString();
    }

    @Override
    public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
      if (object == null) {
        return null;
      }
      if (object instanceof Vuelo) {
        Vuelo o = (Vuelo) object;
        return getStringKey(o.getVueloPK());
      } else {
        throw new IllegalArgumentException("object " + object + " is of type " + object.getClass().getName() + "; expected type: " + Vuelo.class.getName());
      }
    }

  }

}
