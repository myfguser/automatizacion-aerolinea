/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RestFull.WebServices.service;

import entities.VueloHasTarifa;
import entities.VueloHasTarifaPK;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.PathSegment;

/**
 *
 * @author ASSUS
 */
@Stateless
@Path("entities.vuelohastarifa")
public class VueloHasTarifaFacadeREST extends AbstractFacade<VueloHasTarifa> {
  @PersistenceContext(unitName = "TechDemoPU")
  private EntityManager em;

  private VueloHasTarifaPK getPrimaryKey(PathSegment pathSegment) {
    /*
     * pathSemgent represents a URI path segment and any associated matrix parameters.
     * URI path part is supposed to be in form of 'somePath;id=idValue;vueloNroRef=vueloNroRefValue;tarifaId=tarifaIdValue'.
     * Here 'somePath' is a result of getPath() method invocation and
     * it is ignored in the following code.
     * Matrix parameters are used as field names to build a primary key instance.
     */
    entities.VueloHasTarifaPK key = new entities.VueloHasTarifaPK();
    javax.ws.rs.core.MultivaluedMap<String, String> map = pathSegment.getMatrixParameters();
    java.util.List<String> id = map.get("id");
    if (id != null && !id.isEmpty()) {
      key.setId(new java.lang.Integer(id.get(0)));
    }
    java.util.List<String> vueloNroRef = map.get("vueloNroRef");
    if (vueloNroRef != null && !vueloNroRef.isEmpty()) {
      key.setVueloNroRef(new java.lang.Integer(vueloNroRef.get(0)));
    }
    java.util.List<String> tarifaId = map.get("tarifaId");
    if (tarifaId != null && !tarifaId.isEmpty()) {
      key.setTarifaId(new java.lang.Integer(tarifaId.get(0)));
    }
    return key;
  }

  public VueloHasTarifaFacadeREST() {
    super(VueloHasTarifa.class);
  }

  @POST
  @Override
  @Consumes({"application/xml", "application/json"})
  public void create(VueloHasTarifa entity) {
    super.create(entity);
  }

  @PUT
  @Path("{id}")
  @Consumes({"application/xml", "application/json"})
  public void edit(@PathParam("id") PathSegment id, VueloHasTarifa entity) {
    super.edit(entity);
  }

  @DELETE
  @Path("{id}")
  public void remove(@PathParam("id") PathSegment id) {
    entities.VueloHasTarifaPK key = getPrimaryKey(id);
    super.remove(super.find(key));
  }

  @GET
  @Path("{id}")
  @Produces({"application/xml", "application/json"})
  public VueloHasTarifa find(@PathParam("id") PathSegment id) {
    entities.VueloHasTarifaPK key = getPrimaryKey(id);
    return super.find(key);
  }

  @GET
  @Override
  @Produces({"application/xml", "application/json"})
  public List<VueloHasTarifa> findAll() {
    return super.findAll();
  }

  @GET
  @Path("{from}/{to}")
  @Produces({"application/xml", "application/json"})
  public List<VueloHasTarifa> findRange(@PathParam("from") Integer from, @PathParam("to") Integer to) {
    return super.findRange(new int[]{from, to});
  }

  @GET
  @Path("count")
  @Produces("text/plain")
  public String countREST() {
    return String.valueOf(super.count());
  }

  @Override
  protected EntityManager getEntityManager() {
    return em;
  }
  
}
