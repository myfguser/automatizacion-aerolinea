/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RestFull.WebServices.service;

import entities.Vuelo;
import entities.VueloPK;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.PathSegment;

/**
 *
 * @author ASSUS
 */
@Stateless
@Path("entities.vuelo")
public class VueloFacadeREST extends AbstractFacade<Vuelo> {
  @PersistenceContext(unitName = "TechDemoPU")
  private EntityManager em;

  private VueloPK getPrimaryKey(PathSegment pathSegment) {
    /*
     * pathSemgent represents a URI path segment and any associated matrix parameters.
     * URI path part is supposed to be in form of 'somePath;nroRef=nroRefValue;ciudadOrigen=ciudadOrigenValue;ciudadDestino=ciudadDestinoValue'.
     * Here 'somePath' is a result of getPath() method invocation and
     * it is ignored in the following code.
     * Matrix parameters are used as field names to build a primary key instance.
     */
    entities.VueloPK key = new entities.VueloPK();
    javax.ws.rs.core.MultivaluedMap<String, String> map = pathSegment.getMatrixParameters();
    java.util.List<String> nroRef = map.get("nroRef");
    if (nroRef != null && !nroRef.isEmpty()) {
      key.setNroRef(new java.lang.Integer(nroRef.get(0)));
    }
    java.util.List<String> ciudadOrigen = map.get("ciudadOrigen");
    if (ciudadOrigen != null && !ciudadOrigen.isEmpty()) {
      key.setCiudadOrigen(new java.lang.Integer(ciudadOrigen.get(0)));
    }
    java.util.List<String> ciudadDestino = map.get("ciudadDestino");
    if (ciudadDestino != null && !ciudadDestino.isEmpty()) {
      key.setCiudadDestino(new java.lang.Integer(ciudadDestino.get(0)));
    }
    return key;
  }

  public VueloFacadeREST() {
    super(Vuelo.class);
  }

  @POST
  @Override
  @Consumes({"application/xml", "application/json"})
  public void create(Vuelo entity) {
    super.create(entity);
  }

  @PUT
  @Path("{id}")
  @Consumes({"application/xml", "application/json"})
  public void edit(@PathParam("id") PathSegment id, Vuelo entity) {
    super.edit(entity);
  }

  @DELETE
  @Path("{id}")
  public void remove(@PathParam("id") PathSegment id) {
    entities.VueloPK key = getPrimaryKey(id);
    super.remove(super.find(key));
  }

  @GET
  @Path("{id}")
  @Produces({"application/xml", "application/json"})
  public Vuelo find(@PathParam("id") PathSegment id) {
    entities.VueloPK key = getPrimaryKey(id);
    return super.find(key);
  }

  @GET
  @Override
  @Produces({"application/xml", "application/json"})
  public List<Vuelo> findAll() {
    return super.findAll();
  }

  @GET
  @Path("{from}/{to}")
  @Produces({"application/xml", "application/json"})
  public List<Vuelo> findRange(@PathParam("from") Integer from, @PathParam("to") Integer to) {
    return super.findRange(new int[]{from, to});
  }

  @GET
  @Path("count")
  @Produces("text/plain")
  public String countREST() {
    return String.valueOf(super.count());
  }

  @Override
  protected EntityManager getEntityManager() {
    return em;
  }
  
}
