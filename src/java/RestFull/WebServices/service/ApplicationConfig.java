/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RestFull.WebServices.service;

import java.util.Set;
import javax.ws.rs.core.Application;

/**
 *
 * @author ASSUS
 */
@javax.ws.rs.ApplicationPath("webresources")
public class ApplicationConfig extends Application {

  @Override
  public Set<Class<?>> getClasses() {
    Set<Class<?>> resources = new java.util.HashSet<>();
    addRestResourceClasses(resources);
    return resources;
  }

  /**
   * Do not modify addRestResourceClasses() method.
   * It is automatically populated with
   * all resources defined in the project.
   * If required, comment out calling this method in getClasses().
   */
  private void addRestResourceClasses(Set<Class<?>> resources) {
    resources.add(RestFull.WebServices.service.AerolineaFacadeREST.class);
    resources.add(RestFull.WebServices.service.CiudadFacadeREST.class);
    resources.add(RestFull.WebServices.service.EstadoVuelosFacadeREST.class);
    resources.add(RestFull.WebServices.service.PaisFacadeREST.class);
    resources.add(RestFull.WebServices.service.ReservaFacadeREST.class);
    resources.add(RestFull.WebServices.service.SillasVuelosConfirmadosFacadeREST.class);
    resources.add(RestFull.WebServices.service.TarifaFacadeREST.class);
    resources.add(RestFull.WebServices.service.TarifasAerolineaFacadeREST.class);
    resources.add(RestFull.WebServices.service.TipoDocumentoFacadeREST.class);
    resources.add(RestFull.WebServices.service.UsuarioFacadeREST.class);
    resources.add(RestFull.WebServices.service.VistaConsultaHorariosFacadeREST.class);
    resources.add(RestFull.WebServices.service.VistaConsultaTarifaFacadeREST.class);
    resources.add(RestFull.WebServices.service.VistaConsultaVuelosFacadeREST.class);
    resources.add(RestFull.WebServices.service.VueloFacadeREST.class);
    resources.add(RestFull.WebServices.service.VueloHasEstadoVuelosFacadeREST.class);
    resources.add(RestFull.WebServices.service.VueloHasTarifaFacadeREST.class);
    resources.add(RestFull.WebServices.service.VuelosSinCerrarFacadeREST.class);
  }
  
}
