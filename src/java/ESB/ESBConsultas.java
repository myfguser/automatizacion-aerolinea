/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ESB;

import RestFull.WebServices.service.VistaConsultaHorariosFacadeREST;
import RestFull.WebServices.service.VistaConsultaTarifaFacadeREST;
import RestFull.WebServices.service.VistaConsultaVuelosFacadeREST;
import entities.VistaConsultaHorarios;
import java.util.List;
import java.util.stream.Stream;
import javax.ejb.EJB;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;

/**
 *
 * @author ASSUS
 */
@WebService(serviceName = "NewWebService")
public class ESBConsultas {
  /*
   Se invoca los EJB correspondientes a las vistas del modelo
   */

  @EJB
  private VistaConsultaVuelosFacadeREST vistaConsultaVuelosFacadeREST;
  @EJB
  private VistaConsultaTarifaFacadeREST vistaConsultaTarifaFacadeREST;
  @EJB
  private VistaConsultaHorariosFacadeREST vistaConsultaHorariosFacadeREST;

 /*
  Se define la operación de busqueda por ciudades
  */
  @WebMethod(operationName = "consultaHorarioVuelos")
  public List<VistaConsultaHorarios> consultaHorarioVuelos(
          @WebParam(name = "idCiudaOrigen") int idCiudadOrigen,
          @WebParam(name = "idCiudaDestino") int idCiudadDestino
  ) {
    List<VistaConsultaHorarios> listaConsultaVuelos = vistaConsultaHorariosFacadeREST.findAll();
    List<VistaConsultaHorarios> resultado = (List<VistaConsultaHorarios>) listaConsultaVuelos.stream()
            .filter(p -> p.getIdCiudadOrigen() == idCiudadOrigen)
            .filter(p -> p.getIdCiudadDestino() == idCiudadDestino);
    return resultado;
  }
}
