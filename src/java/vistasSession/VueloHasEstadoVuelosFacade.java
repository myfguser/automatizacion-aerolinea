/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vistasSession;

import entities.VueloHasEstadoVuelos;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author ASSUS
 */
@Stateless
public class VueloHasEstadoVuelosFacade extends AbstractFacade<VueloHasEstadoVuelos> {
  @PersistenceContext(unitName = "TechDemoPU")
  private EntityManager em;

  @Override
  protected EntityManager getEntityManager() {
    return em;
  }

  public VueloHasEstadoVuelosFacade() {
    super(VueloHasEstadoVuelos.class);
  }
  
}
