/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vistasSession;

import entities.VueloHasTarifa;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author ASSUS
 */
@Stateless
public class VueloHasTarifaFacade extends AbstractFacade<VueloHasTarifa> {
  @PersistenceContext(unitName = "TechDemoPU")
  private EntityManager em;

  @Override
  protected EntityManager getEntityManager() {
    return em;
  }

  public VueloHasTarifaFacade() {
    super(VueloHasTarifa.class);
  }
  
}
