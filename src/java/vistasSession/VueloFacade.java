/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vistasSession;

import entities.Vuelo;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author ASSUS
 */
@Stateless
public class VueloFacade extends AbstractFacade<Vuelo> {
  @PersistenceContext(unitName = "TechDemoPU")
  private EntityManager em;

  @Override
  protected EntityManager getEntityManager() {
    return em;
  }

  public VueloFacade() {
    super(Vuelo.class);
  }
  
}
